using System;
using System.Linq;
using LibraryAPI.Core.Repositories;
using LibraryAPI.Data.DbContexts;
using LibraryAPI.Data.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace LibraryAPI.Tests
{
    public class BookRepositoryTests
    {
        private DbContextOptions _options;
        private SqliteConnection _connection;
        
        [SetUp]
        public void Setup()
        {
            _connection?.Close();
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
            _options = new DbContextOptionsBuilder<LibraryAPIContext>().UseSqlite(_connection).Options;
            using var context = new LibraryAPIContext(_options);
            context.Database.EnsureCreated();
        }
        
        [Test]
        public void CreateOneShouldFailOnNull()
        {
            using var context = new LibraryAPIContext(_options);
            var repository = new BooksRepository(context);
            Assert.Catch<ArgumentNullException>(() => repository.CreateOne(null));
        }
        
        [Test]
        public void CreateOneShouldWork()
        {
            using var context =  new LibraryAPIContext(_options);
            var rowCount = context.Books.Count();
            var repository = new BooksRepository(context);
            var author = new Author
            {
                FirstName = "Michelle",
                LastName = "Obama",
                Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0")
            };
            var book = new Book
            {
                Title = "Becoming",
                Description = "Becoming is the memoir of former United States first lady Michelle Obama published in 2018",
                Author = author ,
                AuthorId = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0"),
                AmountOfPages = 12
            };
            repository.CreateOne(book);
            repository.Commit();
            Assert.AreEqual(++rowCount, context.Books.Count());
            context.Database.EnsureDeleted();
        }
        
        [Test]
        public void GetOneShouldReturnNullOnWrongId()
        {
            using var context = new LibraryAPIContext(_options);
            var repository = new BooksRepository(context);
            var book = new Book
            {
                Title = "Becoming",
                Description = "Becoming is the memoir of former United States first lady Michelle Obama published in 2018",
                Author = new Author()
                {
                    FirstName = "Michelle",
                    LastName = "Obama",
                    Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0")
                } ,
                AuthorId = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0"),
                AmountOfPages = 12
            };
            repository.CreateOne(book);
            repository.Commit();
            var expectedBook = repository.GetOne(Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b30"));
            Assert.Null(expectedBook);
        }

        [Test]
        public void GetOneShouldReturnSameBook()
        {
            var expectedBook = new Book
            {
                Title = "Becoming",
                Description = "Becoming is the memoir of former United States first lady Michelle Obama published in 2018",
                Author = new Author()
                {
                    FirstName = "Michelle",
                    LastName = "Obama",
                    Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0")
                } ,
                AuthorId = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd7003cb0"),
                AmountOfPages = 12
            };
            using var context =  new LibraryAPIContext(_options);
            var repository = new BooksRepository(context);
            repository.CreateOne(expectedBook);
            repository.Commit();
            var book = repository.GetOne(expectedBook.Id);
            Assert.AreEqual(expectedBook,book);
        }
        
        
        
    }
}