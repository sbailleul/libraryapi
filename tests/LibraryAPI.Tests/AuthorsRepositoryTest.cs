using System;
using System.Linq;
using LibraryAPI.Core.Repositories;
using LibraryAPI.Data.DbContexts;
using LibraryAPI.Data.Entities;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace LibraryAPI.Tests
{
    public class AuthorsRepositoryTest
    {
        private DbContextOptions _options;
        private SqliteConnection _connection;

        [SetUp]
        public void Setup()
        {
            _connection?.Close();
            _connection = new SqliteConnection("DataSource=:memory:");
            _connection.Open();
            _options = new DbContextOptionsBuilder<LibraryAPIContext>().UseSqlite(_connection).Options;
            using var context = new LibraryAPIContext(_options);
            context.Database.EnsureCreated();
        }

        [Test]
        public void CreateOneShouldFailOnNull()
        {
            using var context = new LibraryAPIContext(_options);
            var repository = new AuthorsRepository(context);
            
            Assert.Catch<ArgumentNullException>(() => repository.CreateOne(null));
        }
        
        [Test]
        public void CreateOneShouldWork()
        {
            using var context =  new LibraryAPIContext(_options);
            var rowCount = context.Authors.Count();
            var repository = new AuthorsRepository(context);
            var author = new Author
            {
                FirstName = "Ursula",
                LastName = "Le Guin",
                Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd700abc2")
            };
            
            repository.CreateOne(author);
            repository.Commit();
            Assert.AreEqual(++rowCount, context.Authors.Count());
            context.Database.EnsureDeleted();
        }

        [Test]
        public void GetOneShouldReturnNullOnWrongId()
        {
            using var context = new LibraryAPIContext(_options);
            var repository = new AuthorsRepository(context);
            var author = new Author
            {
                FirstName = "Ursula",
                LastName = "Le Guin",
                Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd700abc2")
            };
            repository.CreateOne(author);
            repository.Commit();
            var expectedAuthor = repository.GetOne(Guid.Parse("d28888e9-2ba9-473a-a40f-e38cb54f9b30"));
            Assert.Null(expectedAuthor);
        }

        [Test]
        public void GetOneShouldReturnSameAuthor()
        {
            var expectedAuthors = new Author
            {
                FirstName = "Ursula",
                LastName = "Le Guin",
                Id = Guid.Parse("0ffc10d6-741a-481a-bd56-32afd700abc2")
            };

            using var context =  new LibraryAPIContext(_options);
            var repository = new AuthorsRepository(context);
            repository.CreateOne(expectedAuthors);
            repository.Commit();
            var actualAuthor = repository.GetOne(expectedAuthors.Id);
            Assert.AreEqual(expectedAuthors,actualAuthor);
        }
    }
}