
using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryAPI.Core.Repositories;
using LibraryAPI.Data.Entities;
using LibraryAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAPI.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BooksController : ControllerBase
    {
        private readonly IBooksRepository _booksRepository;
        private readonly IAuthorsRepository _authorsRepository;
        private readonly IMapper _mapper;

        public BooksController(
            IBooksRepository booksRepository,
            IAuthorsRepository authorsRepository,
            IMapper mapper)
        {
            _booksRepository = booksRepository ?? throw new ArgumentNullException(nameof(booksRepository));
            _authorsRepository = authorsRepository ?? throw new ArgumentNullException(nameof(_authorsRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{bookId}", Name = "[action]")]
        public async Task<IActionResult> GetOneBook(Guid bookId)
        
        
        {
            Book bookEntity = await _booksRepository.GetOneAsync(bookId);

            if (bookEntity == null) { return NotFound();}

            Author authorEntity = await _authorsRepository.GetOneAsync(bookEntity.AuthorId);

            bookEntity.Author = authorEntity;
            
            var bookDto = _mapper.Map<BookDtoQuery>(bookEntity);
            
            return Ok(bookDto);
        }

        [HttpPost]
        public IActionResult CreateOne([FromBody] BookDtoCommand bookToCreate)
        {
            Book bookEntity = _mapper.Map<Book>(bookToCreate);
            
            Author authorEntity = _authorsRepository.GetOneByFullName(bookToCreate.AuthorFirstname, bookToCreate.AuthorLastname);
            
            if (authorEntity == null)
            {
                Author authorToCreate = new Author
                {
                    FirstName = bookToCreate.AuthorFirstname, LastName = bookToCreate.AuthorLastname
                };
                Console.WriteLine(authorToCreate.FirstName);
                _authorsRepository.CreateOne(authorToCreate);
                _authorsRepository.Commit();
                authorEntity = _authorsRepository.GetOneByFullName(authorToCreate.FirstName, authorToCreate.LastName);
            }
            
            bookEntity.Author = authorEntity;
            _booksRepository.CreateOne(bookEntity);
            _booksRepository.Commit();
            return CreatedAtRoute(nameof(GetOneBook), new {bookId= bookEntity.Id}, _mapper.Map<BookDtoQuery>(bookEntity));
            
        }

        [HttpPost(template: "{bookId}", Name = "[action]")]
        public IActionResult UpdateOneBook(Guid bookId, [FromBody] BookDtoCommand bookUpdated)
        {
            var bookEntity = _mapper.Map<Book>(bookUpdated);
            bookEntity.Id = bookId;
            _booksRepository.UpdateOne(bookEntity);
            _booksRepository.Commit();
            return CreatedAtRoute(nameof(GetOneBook), new {bookId = bookEntity.Id},
                _mapper.Map<BookDtoQuery>(bookEntity));
        }

        [HttpDelete(template: "{bookId}", Name = "[action]")]
        public IActionResult RemoveOneBook(Guid bookId)
        {
            Book bookEntity = _booksRepository.GetOne(bookId);
            if (bookEntity == null)
            {
                return NotFound();
            }

            _booksRepository.RemoveOne(bookEntity);
            _booksRepository.Commit();
            return Ok();
        }
    }
}