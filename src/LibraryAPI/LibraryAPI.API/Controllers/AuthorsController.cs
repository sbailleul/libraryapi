using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using LibraryAPI.Core.Repositories;
using LibraryAPI.Data.Entities;
using LibraryAPI.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryAPI.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AuthorsController : ControllerBase
    {
        private readonly IAuthorsRepository _authorsRepository;
        private readonly IBooksRepository _booksRepository;
        private readonly IMapper _mapper;

        public AuthorsController(
            IAuthorsRepository authorsRepository, 
            IBooksRepository booksRepository, 
            IMapper mapper)
        {
            _authorsRepository = authorsRepository ?? throw new ArgumentNullException(nameof(authorsRepository));
            _booksRepository = booksRepository ?? throw new ArgumentNullException(nameof(booksRepository));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet("{authorId}", Name = "[action]")]
        public async Task<IActionResult> GetOneAuthor(Guid authorId, [FromQuery] Boolean includeBooks )
        
        
        {
            Author authorEntity = await _authorsRepository.GetOneAsync(authorId);

            if (authorEntity == null) { return NotFound();}

            if (includeBooks)
            {
                var books = await _booksRepository.GetAllAsyncByAuthor(authorId);
                authorEntity.Books = books.ToList();
            }
            

            var authorDto = _mapper.Map<AuthorDtoQuery>(authorEntity);
            return Ok(authorDto);
        }
        
        
        [HttpGet]
        public async Task<IActionResult> GetAllAuthors([FromQuery] string firstname,
            [FromQuery] string lastname)
        {
            IEnumerable<Author> authorEntities = await _authorsRepository.GetAllAsync();

            authorEntities = authorEntities
                .Where(a => firstname == null || a.FirstName.ToLower().StartsWith(firstname.ToLower()))
                .Where(a => lastname == null || a.LastName.ToLower().StartsWith(lastname.ToLower()))
                .OrderBy(a => a.LastName);

            if (!authorEntities.Any())
            {
                return NotFound();
            }
            
            return Ok(_mapper.Map<IEnumerable<AuthorDtoQuery>>(authorEntities));

        }

        [HttpPost]
        public IActionResult CreateOne([FromBody] AuthorDtoCommand authorToCreate)
        {
            var authorEntity = _mapper.Map<Author>(authorToCreate);
            _authorsRepository.CreateOne(authorEntity);
            _authorsRepository.Commit();
            return CreatedAtRoute(nameof(GetOneAuthor), new {authorId= authorEntity.Id}, _mapper.Map<AuthorDtoQuery>(authorEntity));
        }
    }
}