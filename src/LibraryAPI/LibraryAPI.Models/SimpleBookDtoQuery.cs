using System;
using System.ComponentModel.DataAnnotations;

namespace LibraryAPI.Models
{
    public class SimpleBookDtoQuery
    {
        [Key] public Guid Id { get; set; }

        [Required] [MaxLength(150)] public string Title { get; set; }

        [MaxLength(2500)] public string Description { get; set; }

        public int? AmountOfPages { get; set; }

        public Guid AuthorId { get; set; }
    }
}