using System;

namespace LibraryAPI.Models
{
    public class SimpleAuthorDtoQuery
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
    }
}