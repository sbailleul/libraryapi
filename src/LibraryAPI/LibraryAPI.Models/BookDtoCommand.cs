using System.ComponentModel.DataAnnotations;

namespace LibraryAPI.Models
{
    public class BookDtoCommand
    {
        [Required] [MaxLength(150)] public string Title { get; set; }

        [Required] [MaxLength(150)] public string Description { get; set; }
        
        [MaxLength(150)] public string AuthorFirstname { get; set; }
        
        [MaxLength(150)] public string AuthorLastname { get; set;  }
    }
}