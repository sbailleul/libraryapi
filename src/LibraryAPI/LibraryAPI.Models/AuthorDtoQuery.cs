﻿using System;
using System.Collections.Generic;

namespace LibraryAPI.Models
{
    public class AuthorDtoQuery
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }

        public ICollection<SimpleBookDtoQuery> Books { get; set; } = new List<SimpleBookDtoQuery>();
    }
}