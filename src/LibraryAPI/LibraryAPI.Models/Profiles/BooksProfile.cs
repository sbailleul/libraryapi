using AutoMapper;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Models.Profiles
{
    public class BooksProfile: Profile
    {
        public BooksProfile()
        {
            CreateMap<Book, BookDtoQuery>()
                .ForMember(dest => dest.Title,
                    opt => opt.MapFrom(src => $"{src.Title} {src.Author.FirstName} {src.Author.LastName}"));

            CreateMap<Book, SimpleBookDtoQuery>()
                .ForMember(
                    dest => dest.Title,
                    opt => opt.MapFrom(src => $"{src.Title} {src.Author.FirstName} {src.Author.LastName}"));
            
            CreateMap<BookDtoCommand, Book>();
            
        }
    }
}