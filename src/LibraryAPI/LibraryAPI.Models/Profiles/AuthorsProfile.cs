using AutoMapper;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Models.Profiles
{
    public class AuthorsProfile : Profile
    {
        public AuthorsProfile()
        {
            CreateMap<AuthorDtoCommand, Author>();
            
            CreateMap<Author, AuthorDtoQuery>()
                .ForMember(
                    dest => dest.FullName,
                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"));

            CreateMap<Author, SimpleAuthorDtoQuery>()
                .ForMember(
                    dest => dest.FullName,
                    opt => opt.MapFrom(src => $"{src.FirstName} {src.LastName}"));
                ;
        }
    }
}