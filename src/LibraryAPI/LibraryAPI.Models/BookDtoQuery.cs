using System;
using System.ComponentModel.DataAnnotations;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Models
{
    public class BookDtoQuery
    {
        [Key] public Guid Id { get; set; }

        [Required] [MaxLength(150)] public string Title { get; set; }

        [MaxLength(2500)] public string Description { get; set; }

        public int? AmountOfPages { get; set; }

        public Guid AuthorId { get; set; }
        public SimpleAuthorDtoQuery Author { get; set; }
    }
}