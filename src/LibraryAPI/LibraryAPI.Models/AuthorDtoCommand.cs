using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Models
{
    public class AuthorDtoCommand
    {
        [Required] [MaxLength(150)] public string FirstName { get; set; }

        [Required] [MaxLength(150)] public string LastName { get; set; }
    }
}