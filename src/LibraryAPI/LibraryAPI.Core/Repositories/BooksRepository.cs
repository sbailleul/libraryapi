using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LibraryAPI.Data.DbContexts;
using LibraryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace LibraryAPI.Core.Repositories
{
    public class BooksRepository : IBooksRepository
    {
    
        private readonly LibraryAPIContext _context;

        public BooksRepository(LibraryAPIContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }
        
        public async Task<bool> CommitAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
        
        public async Task<Book> GetOneAsync(Guid bookId)
        {
            return await _context.Books.FirstOrDefaultAsync(b => b.Id == bookId);
        }
        
        public async Task<IEnumerable<Book>> GetAllAsyncByAuthor(Guid authorId)
        {
            return await _context.Books.Where( b => b.AuthorId == authorId).ToListAsync();
        }

        public void CreateOne(Book book)
        {
            if (book == null) throw new ArgumentNullException(nameof(book));
            book.Id = Guid.NewGuid();
            _context.Add(book);        
        }

        public bool Commit()
        {
            return  _context.SaveChanges() > 0;        
        }

        public void UpdateOne(Book book)
        {
            if (book == null) throw new ArgumentNullException(nameof(book));
            _context.Update(book);
        }

        public Book GetOne(Guid bookId)
        {
            return _context.Books.FirstOrDefault(b => b.Id == bookId);
        }

        public void RemoveOne(Book bookEntity)
        {
            _context.Remove(bookEntity);
        }
    }
}