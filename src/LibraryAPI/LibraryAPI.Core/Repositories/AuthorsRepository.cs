using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using LibraryAPI.Data.DbContexts;
using LibraryAPI.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace LibraryAPI.Core.Repositories
{

    public class AuthorsRepository : IAuthorsRepository
    {
        private readonly LibraryAPIContext _context;

        public AuthorsRepository(LibraryAPIContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Author> GetOneAsync(Guid authorId)
        {
            return await _context.Authors.FirstOrDefaultAsync(a => a.Id == authorId);
        }
        
        public async Task<IEnumerable<Author>> GetAllAsync()
        {
            return await _context.Authors.ToListAsync();
        }

        public async Task<bool> CommitAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }
        
        public Author GetOne(Guid authorId)
        {
            return  _context.Authors.FirstOrDefault(a => a.Id == authorId);
        }
        
        public  IEnumerable<Author> GetAll()
        {
            return  _context.Authors.ToList();
        }

        public bool Commit()
        {
            return  _context.SaveChanges() > 0;
        }

        public void CreateOne(Author author)
        {
            if (author == null) throw new ArgumentNullException(nameof(author));
            author.Id = Guid.NewGuid();
            _context.Add(author);
        }

        public void UpdateOne(Author author)
        {
            // No code implementation
        }

        public  Author GetOneByFullName(string firstname, string lastname)
        {
            return _context.Authors.FirstOrDefault(a => a.FirstName == firstname && a.LastName == lastname);
        }
    }
}