using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Core.Repositories
{
    public interface IBooksRepository
    {
        Task<bool> CommitAsync();
        Task<Book> GetOneAsync(Guid authorId);
        Task<IEnumerable<Book>> GetAllAsyncByAuthor(Guid authorId);
        void CreateOne(Book bookEntity);
        bool Commit();
        void UpdateOne(Book bookEntity);
        Book GetOne(Guid bookId);
        void RemoveOne(Book bookEntity);
    }
}