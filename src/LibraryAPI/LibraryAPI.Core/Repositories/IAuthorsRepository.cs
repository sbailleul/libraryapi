using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LibraryAPI.Data.Entities;

namespace LibraryAPI.Core.Repositories
{
    public interface IAuthorsRepository
    {
        Task<Author> GetOneAsync(Guid authorId);
        Task<IEnumerable<Author>> GetAllAsync();
        Task<bool> CommitAsync();
        Author GetOne(Guid authorId);
        IEnumerable<Author> GetAll();
        bool Commit();
        void CreateOne(Author author);
        void UpdateOne(Author author);
        Author GetOneByFullName(string firstname, string lastname);
    }
}