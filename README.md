# Versioning
## Workflow GitFlow 
Utilisation de gitflow, ce workflow nous permet la mise en place de 4 types de branches principales : 

* Master unique branche de production contenant des versions officielles et fonctionnelles de notre projet
* Release multiple branches dédiées pour toutes les modifications avant livraison en production ne présentant pas d’ajout de fonctionnalités, correction de bug, tests etc… Ces branches seront basées sur la branche develop et auront pour tag la nouvelle version à livrer. 
* Develop unique branche dédiée aux intégrations de nouvelles fonctionnalitées au fur et à mesure.
* Features multiple branches dédiées à chacune à une fonctionnalité, ces branches basées sur la branch develop seront merge avec develop une fois leur fonctionnalité prête;
 
Il est possible d’envisager des branches de type hotfix en cas d’urgence si une version de la branche master comporte des bugs non décelés dans les branches citées ci-dessus. 

Les merge requests porteront sur les ajouts des branches release vers la branche master ainsi que sur les ajouts des branches features vers la branche develop.

Le responsable des merge request n’est pas encore désigné. 

## Merge requests
Toutes les merges requests créeront des merges commit. 

##  Convention commits
Utilisation de la convention adoptée par Angular  
``` 
    <type>(<portée>): <sujet>
    
    <description>
    
    <footer>
```
Lien pour plus d'informations : https://buzut.net/git-bien-nommer-ses-commits/

# Fonctionnalités de l’application
L'application sera une API réalisée avec ASP.Net Core 3.1. Cette API sera connecté à une base de donnée SQL Server édition Epress. 
L'application permettra de gérer des autheurs et des livres. Les opérations effectuées sur ces entités seront de type CRUD. 

# Contribuer

## Prérequis
Avoir Docker  19.03.3 ou plus => https://docs.docker.com/install/
Avoir Docker compose 1.25.3 ou plus => https://docs.docker.com/compose/install/


## Optionnel
Pour éxécuter l'application en local :
* Avoir le SDK ASP.Net Core 3.1 => https://dotnet.microsoft.com/download 

Pour générer des migrations de base de données :
* Avoir EntityFramework cli : 
```
    dotnet tool install -g dotnet-ef
```
### Générer une migration 
Se placer dans le répertoire src/LibraryAPI/LibraryAPI.Data et faire la commande :
```
dotnet-ef migrations add InitialMigration -s ../LibraryAPI.API/
```
## Recupération des sources
Effectuer un clone du dépot distant via https://gitlab.com/sbailleul/libraryapi.git

## Exécution de l'environnement de développement
### Avec Docker Compose
Pour démarrer l'environnement de développement il faudra se placer dans la racine du dépot. 
Exécuter ensuite la commande suivante pour builds l'image sbailleul/library_api_dev:latest : 
```
docker-compose -f ./Docker/dev/docker-compose.yml build
```
Exécuter ensuite la commande suivante pour démarrer les images de l'environnement de développement : 
```
docker-compose -f ./Docker/dev/docker-compose.yml up
```
Cette commande générera deux containers, un pour l'application ayant son working directory bindé sur le répertoire ./src et un pour la base de données SQL Server

### Avec IDE Rider
#### Activé l'écoute tcp sur docker daemon
Dans un shell faire :
```
sudo dockerd -H tcp://0.0.0.0:
```
Pour conserver la configuration éditer le fichier __/lib/systemd/system/docker.service__ en rajoutant l'option __-H tcp://0.0.0.0:__ à la fin de la ligne suivante :
```
ExecStart=/usr/bin/dockerd
```
La modification dans le fichier docker.service sera appliquée à chaque démarrage de docker daemon. 

Pour lancer docker daemon à chaque démarrage du système sur Ubuntu 15+ faire : 
```
sudo systemctl enable docker
```

#### Configurer Rider 
Accéder aux paramètres Docker en vous rendant dans File -> Settings -> Build, Execution, Deployment -> Docker
Ajouter si besoin avec le bouton + une connexion docker. 
Dans l'actuelle/nouvelle connexion activer l'option "TCP socket". 
Dans le champ "Engine API URL" situé sous l'option "TCP socket" saisir la chaine de caractère suivante : tcp://localhost:2375
Lors de la modification une tentative de connexion sera effectuée, l'état de la connexion doit passer de "Connecting..." à "Connection succesfull".

Si la connexion ne fonctionne pas, redémarrer le service docker daemon avec la commande suivante et réessayer : 
```
sudo systemctl restart docker.service 
```

#### Exécution
Builder et déployer l'image de la base de données : 
```
docker-compose -f ./Docker/dev/docker-compose.yml build sql_server_express_db
docker-compose -f ./Docker/dev/docker-compose.yml up sql_server_express_db
```
Pour builder et déployer l'image applicative utiliser la configuration Docker library_api_dev de Rider localisée ici :
__/home/sbailleul/Documents/Code/Projects/LibraryAPI/src/LibraryAPI/.idea/.idea.LibraryAPI/.idea/runConfigurations/library_api_dev.xml__ 

Le build de l'image applicative et le démarrage de son container sera géré par Rider, cette configuration permet aussi de démarrer l'application avec l'outil de débogage de Rider. 

## Build et exécution de l'application dans le container library_api_dev
Exécuter la commande suivante pour accéder aux shell du container
```
docker exec -i library_api_dev bash
```
Pour builder le projet exécuter la commande suivante : 
```
dotnet publish  /src/LibraryAPI/LibraryAPI.API -o /src/build
```
Pour exécuter l'application buildée faire la commande suivante en étant dans le répertoire /src/build : 
```
dotnet LibraryAPI.API.dll
```

# Déploiement

Utilisation de Gitlab CI comme outil d'intégration continue. Nous avons choisi cette outil car il nous permet de gérer simplement des pipelines à la fois pour les tests et pour le déploiement. Pour utiliser Gitlab CI un gitlab-runner a été créé sur la machine de Sacha BAILLEUL. 

Utilisation d'Heroku pour héberger la version de production de notre application afin de garantir la disponibilité de notre service en continu.

## Gitlab-runner
Installation du gitlab-runner sur une machine Ubuntu dans le cadre de notre projet : 
* Ajout du repository GitLab
```
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
```
* Installation de la dernière version de gitlab-runner
```
sudo apt-get install gitlab-runner
```
* Création du runner
```
sudo gitlab-runner register
```
* Ajout des informations d'enregistrement présentes dans les paramètres de projet (Settings -> CI/CD -> Runners:Expand -> Set up a specific Runner manually)
    * Ajout de l'url gitlab 
    ```
    Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
    https://gitlab.com
    ```
    * Ajout du token de projet 
    ```
    Please enter the gitlab-ci token for this runner
    xxx
    ```
* Ajout d'une description 
```
Please enter the gitlab-ci description for this runner
Runner to build, test and deploy application
```
* Ajout d'étiquettes
```
Please enter the gitlab-ci tags for this runner (comma separated):
LibraryAPI_CI/CD
```
* Choix de l'exécuteur docker 
```
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker
```
* Choix de l'image docker par défaut 
```
Please enter the Docker image (eg. ruby:2.6):
docker:19.03.3
```

## Heroku
Pour héberger notre application sur Heroku nous avons créer un compte en ligne. Sur ce compte nous avons créer l'application library-api-esgi. Pour exécuter note application nous avons mis en place une dyno étiquettée "web" déployant l'image docker de production. 

Installation d'heroku cli sur machine Ubuntu :
```
sudo snap install --classic heroku
```
Connexion à un compte heroku avec navigateur : 
```
heroku login
```
Connexion de docker au repository Heroku : 
```
docker login --username=bailleul.sacha@gmail.com --password=$(heroku auth:token) registry.heroku.com
```
Build de l'image de production : 
```
docker build -f Docker/prod/Dockerfile.prod --tag library_api_prod .
```
Etiquettage de l'image de production pour correspondre avec la dyno Heroku : 
```
docker tag library_api_prod registry.heroku.com/library-api-esgi/web
```
Envoi de l'image buildée sur le repository d'Heroku : 
```
docker push registry.heroku.com/library-api-esgi/web
```
Exécution de la dyno Heroku taggée "web" pour l'application "library-api-esgi" : 
```
heroku container:release web --app library-api-esgi 
```

## Pipeline 
Nous avons choisi un workflow de type docker in docker recommandé par Gitlab. Ce workflow permet à chaque job du pipeline de fonctionné dans un environnement propre, qui sera exécuté grâce à l'image docker:19.03.3-dind

### Job build
Ce job build toutes les images nécessaires aux jobs suivant. Toutes les images sont nommées en fonction de la variable $CI_REGISTRY_IMAGE (registry.gitlab.com/sbailleul/libraryapi) cependant elles ont toutes un tag différent :
* base, est utilisée pour générer la publication de production dans son répertoire /app/prod_build et la publication de tests dans son répertoire /app/tests_build.
* tests, est utilisée pour exécuter les tests unitaires du projet en se basant sur l'image base 
* prod, est utilisée pour exécuter le projet en production en se basant sur l'image de base et en utilisant la variable d'environnement $PORT mis à disposition par Heroku pour spécifier le port d'écoute de l'application. 

Pour optimiser la construction de l'image de base nous utilisons l'option "--cache-from $CI_REGISTRY_IMAGE:base" qui permet de ne reconstruire que les les images intermédiaires différentes entre la nouvelle et l'ancienne version de l'image. 
  
### Job tests
Si le job de build est exécuté avec succès, ce job exécute les tests de l'image de test à partir du point d'entrée : 
```
"/usr/share/dotnet/dotnet", "test","./tests_build/LibraryAPI.Tests.dll
```
Ce job ne sera exécuté que sur les branches suivantes : 
* develop
* toutes les branches commencant par : 
    * release/
    * features/
    * refactor/
    * test/
    * fix/
  
### Job déploiement

Si le job de tests est exécuté avec succès, ce job exécute le déploiement de l'image de production sur Heroku à partir de la commande : 
```
ASPNETCORE_URLS=http://*:$PORT  dotnet prod_build/LibraryAPI.API.dll
```
Cette commande modifie le port d'écoute de l'application en utilisant la variable d'environnement $PORT fournit par Heroku et exécute l'application.

Ce job ne sera exécuté que sur la branche master.

Les variables d'environnement suivantes sont utilisées dans le cadre du déploiement sur Heroku : 
* $HEROKU_TOKEN = Clé pour se connecter sur le compte de gestionnaire de l'application Heroku;
* $HEROKU_IMG = Nom de l'image déployé sur le repository Heroku;
* $HEROKU_APP = Nom de l'application hébergée par Heroku;

Pour déployer l'image de prod sur Heroku il est nécessaire de : 
* Tagger l'image de prod avec le tag correspondant à l'image du repository Heroku ;
* Envoyer l'image sur le repository Heroku ; 
* Installer curl ;
* Envoyer une requête de release sur l'API d'Heroku.


# Tests logiciels
Pour faire un build de test en étant à la racine du projet faire : 
```
dotnet publish tests/LibraryAPI.Tests -o tests/build
```
Pour exécuter les tests faire au choix : 
* Une publication des tests dans le répertoire ./tests/build et ensuite la commande : 
```
dotnet test tests/build/LibraryAPI.Tests.dll
```
* L'éxécution de la commande :
```
dotnet test tests/LibraryAPI.Tests/LibraryAPI.Tests.csproj
```

